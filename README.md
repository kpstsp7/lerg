# LERG upload/download app #

Flask based app for upload and download LERG files

## Features:
* simple API for get history and files
* enable/disable specify file download, limitation by download quantity from unique IP
* all uploaded files also stored on google drive and links provide via web UI


### Requirements\Dependencies ###

* Python: Python 2.7
* all needed pip packages listed in requirements.txt

### Installation

* Notice: use deploy script for start app 
```
sh deploy.sh
```
Script will run create virtualenv activate it and install all necessary packages if it need, and after will run web app.


### Configuration ###

All needable constants you can find in lerg.py at begin of file.
All constants wrote in uppercase.
Configurable constants include login\password 

### API description ###

-       Get_Last_Lerg_Time
	  * This will return the latest lergs
-       Get_LatestLerg
	  * This will return the content of the latest lerg in a specific format.
	  * Each IP can’t request more than 5 times in one day.  If exceeded, we will return 404 and “You have exceeded maximum limit of 5 downloads from IP x.x.x.x.
We need to make sure that this API can handle a large number of request daily.

```
curl -X GET "http://192.99.10.113:8000/Get_Last_Lerg_Time"

```

>[{"uploaded": "2017-06-24-19:57:25", "download_cnt": 0, "record_cnt": 1, 
"file_name": "lerg_test111_7.txt", "goopath": "https://drive.google.com/file/d/0BwhXC0uZxUsXaXBOVEdpRHJPV3c/view?usp=drivesdk", "id": 1}]

```

curl -X GET "http://192.99.10.113:8000/Get_LatestLerg"

```

>test




### Folder strucure ###

* 'uploads' directory containts uploaded files.
static and templates part of webb app
		
		⚡ tree -d
		├── static
		├── templates
		└── uploads

