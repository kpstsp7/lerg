# all the imports
import sqlite3
from flask import Flask, request, session, g, redirect, url_for, \
     abort, render_template, flash, Response, Request, send_file
from flask_wtf import FlaskForm
import werkzeug.exceptions as ex
from werkzeug.exceptions import HTTPException, NotFound
from flask_wtf.file import FileField, FileRequired, FileAllowed
from wtforms import SubmitField
from contextlib import closing
import flask_uploads
from flask_uploads import UploadSet, configure_uploads, IMAGES, patch_request_class
import datetime
import json
from pydrive.auth import GoogleAuth
from pydrive.drive import GoogleDrive
import os

#from itertools errorhandler
# configuration
DATABASE = './lerg.db'
DEBUG = True
SECRET_KEY = 'development key'
UPLOAD_FOLDER='./uploads'
#TODO: add extensions limmit
#ALLOWED_EXTENSIONS=
USERNAME = 'adm'
PASSWORD = 'LrtMNs1'
EXTENSION = 'txt'

#upload directives
#UPLOADED_FILES_DEST
#UPLOADED_FILES_URL
#UPLOADED_FILES_ALLOW
#UPLOADED_FILES_DENY
#UPLOADS_DEFAULT_DEST
UPLOADED_FILES_DEST = './uploads'
#UPLOADS_DEFAULT_URL



#create simple test
#handle non standart http errors
class errDisableDownload(ex.HTTPException):
    pass




app = Flask(__name__)
app.config.from_object(__name__)

app.config.from_envvar('ITJAM_SETTINGS', silent=True)


files = flask_uploads.UploadSet('files', extensions='txt')

configure_uploads(app, (files,))
patch_request_class(app)  # set maximum file size, default is 16MB


class UploadForm(FlaskForm):
#        ffile = FileField(validators=[FileAllowed(files, u'Lerg files only!'),
            ffile = FileField(FileRequired(u'File was empty!'))
#validators=[FileAllowed(photos, u'Image only!'),
            FileRequired(u'File was empty!')
            submit = SubmitField(u'Upload')


def checkDownCnt(file_name, cur_ip):
    rescount=0
    today = (datetime.datetime.now()+ datetime.timedelta(1)).strftime("%F")
#= datetime.datetime.now().strftime("%F")
    yesterday = (datetime.datetime.now()- datetime.timedelta(1)).strftime("%F")
    sqlcmd = 'select count(srcip) from downloads \
            where endtime BETWEEN "' + yesterday +' 00:00:00" and "' + today + ' \
            23:59:00" and file_name="'+file_name+'" \
            group by file_name'
    print sqlcmd
    cur =  g.db.execute(sqlcmd)
    dncount = cur.fetchall()
    cur.close()
    if len(dncount)>0:
        rescount=dncount[0][0]
    #else:
    #    raise
    return rescount

def gdriveLogin():
    global gauth, drive
    gauth = GoogleAuth()
  #  gauth.LocalWebserverAuth() # Creates local webserver and auto handles authentication
    gauth.LoadCredentialsFile(credentials_file="client_secrets44.json")
    drive = GoogleDrive(gauth) # Create GoogleDrive instance with authenticated GoogleAuth instance

def getGdriveFolderId():
    #drive = GoogleDrive
    id='0'
    file_list = drive.ListFile({'q': "'root' in parents and trashed=false"}).GetList()
    if len(file_list)>0:
        for file1 in file_list:
            if file1['title']!='LERG':
                continue
            else:
                id=file1['id']
        print id
    return id

def find_folders(fldname):
    file_list = drive.ListFile({'q': "title='{}' and mimeType contains 'application/vnd.google-apps.folder' and trashed=false".format(fldname) }).GetList()
    return file_list





def uploadFileGdrive(filename):
    gdriveLogin()
    fid=str(getGdriveFolderId())
    folder = drive.CreateFile({"parents": [{"kind": "drive#fileLink", "id": fid}]})
    upath=os.path.abspath('./uploads')+'/'+filename
    folder.SetContentFile(upath)
    folder.Upload()
    permission = folder.InsertPermission({
                        'type': 'anyone',
                        'value': 'anyone',
                        'role': 'reader'})


def ListFolder(parent):

    filelist=[]
    files=[]
    file_list = drive.ListFile({'q': "'%s' in parents and trashed=false" % parent}).GetList()
    for f in file_list:
      if f['mimeType']=='application/vnd.google-apps.folder': # if folder
          filelist.append({"id":f['id'],"title":f['title'],"list":ListFolder(f['id'])})
          continue
      else:
          filelist.append({"title":f['title'],"title1":f['alternateLink']})


    return filelist




def getLinksFromGdrive(filename):
    filelistListFolder('root')





def checkDisabled(filename):
    cur = g.db.execute("select count(*) from disabled where \
            file='"+filename+"'")
    disblcount = cur.fetchall()[0][0]
    cur.close

    return disblcount





def connect_db():
    return sqlite3.connect(app.config['DATABASE'])

def init_db():
    with closing(connect_db()) as db:
        with app.open_resource('schema.sql', mode='r') as f:
            db.cursor().executescript(f.read())
        db.commit()

def getUploads():
    cur = g.db.execute('select up.id, up.uploaded, up.record_cnt, up.download_cnt, up.goopath, up.file_name, count(dn.file_name) from uploads up left join  downloads dn on up.file_name=dn.file_name group by up.file_name')
#entries
    uploads = [dict(id=row[0], uploaded=row[1],
        record_cnt=row[2], download_cnt=row[6], goopath=row[4],
        file_name=row[5]) for row in cur.fetchall()]
    cur.close()
    return uploads


@app.errorhandler(HTTPException)
def http_error_handler(error):
    return 'Something go wrong'


#@app.errorhandler(drive.ApiRequestError)
#def goo_api_request_error(error):
#    flash("we have some troubles with google acc")

#@app.errorhandler(Exception)
#def http_error_handler(error):
#    flash('Download disabled!')
#    return redirect(url_for('show_entries'))


@app.route('/',  methods=['GET','POST'])
def show_entries():
    cur =  g.db.execute('select count(*) from uploads')
    allcount = cur.fetchall()[0][0]
    cur.close()

    uploads=getUploads()
    file_url=None
    validateRes=None
    form = UploadForm()
    validateRes=form.validate_on_submit()
    if validateRes:
        filename = files.save(form.ffile.data)
        file_url = files.url(filename)
        current_count = allcount+1
#add files on gdrive
        gdriveLogin()
        id=str(getGdriveFolderId())
        folder = drive.CreateFile({"parents": [{"kind": "drive#fileLink", "id": id}]})
        upath=os.path.abspath('./uploads')+'/'+filename
        folder.SetContentFile(upath)
        folder['title']=filename
        try:
            folder.Upload()
            permission = folder.InsertPermission({
                                'type': 'anyone',
                                'value': 'anyone',
                                'role': 'reader'})
            link=''
            ls = ListFolder('root')
            for i in ls[0]['list']:
                if str(i['title'])==filename:
                    link=str(i['title1'])
                    break

            flash('File sucessfully uploaded')
            g.db.execute('insert into uploads (uploaded, record_cnt, download_cnt, \
            goopath, file_name) values (?, ?, ?, ?, ?)' \
            ,[datetime.datetime.now().strftime("%F-%H:%M:%S"), \
            str(current_count), 'empty', link, filename])
            g.db.commit()
            uploads=getUploads()
            return render_template('show_entries.html', form=form, file_url=file_url, uploads=uploads)

        except(Exception) as gooex:
            flash("We have some problems with google drive. File not been uploaded")
            raise
            #pass

    elif (not validateRes) and form.is_submitted():
        file_url = None
        flash('File upload error')
        return render_template('show_entries.html', form=form, file_url=file_url, uploads=uploads)

    return render_template('show_entries.html', form=form, file_url=file_url, uploads=uploads)


@app.route('/file_downloads/<filename>')
def file_downloads(filename):
    ip = request.environ.get('HTTP_X_REAL_IP', request.remote_addr)
    dncount=0
    disblcount = checkDisabled(filename)
    dncount=checkDownCnt(filename, ip)

    if dncount>=5:
#        abort(304, "Download not allowed")
        flash("Download counti from this IP exceeded. Game over")

        return redirect(url_for('show_entries'))
        #return render_template('disable.html'), 304
    elif disblcount>0:
        #        abort(304)
        flash("This file disable for download")
        return redirect(url_for('show_entries'))
        #return render_template(url_for('errorhandler')), 304
    else:
        try:
            start_time = datetime.datetime.now().strftime("%F-%H:%M:%S")
#        headers = {"Content-Disposition": "attachment; filename=%s" % filename}
     #       with open('./uploads/'+filename, 'r') as f:
     #           body = f.read()
            down_finish_time =  datetime.datetime.now().strftime("%F-%H:%M:%S")
#        return response.make_response((body, headers))
            ip = request.environ.get('HTTP_X_REAL_IP', request.remote_addr)
            g.db.execute('insert into downloads (downloaded, starttime, \
            endtime,srcip, status, error, file_name) values (?, ?, ?, ?, ?, ?, \
            ?)',[start_time, start_time, down_finish_time, ip,'test_status', 'not error', filename])
            g.db.commit()
            return send_file('./uploads/'+filename, as_attachment=True)



        except Exception as e:
            down_error = str(e)
            g.db.execute('insert into downloads (downloaded, starttime, \
            endtime,srcip, status, error, file_name) values (?, ?, ?, ?, ?, ?, \
            ?)',[start_time, start_time, down_finish_time, 'None','test_status', \
            str(e), filename])
            g.db.commit()

            return str(e)
    uploads=getUploads()
    return redirect(url_for('show_entries'))

#disable function
@app.route('/disable/<filename>')
def disable(filename):

    disblcount=checkDisabled(filename)

    if disblcount == 0:
        g.db.execute('insert into disabled (file) values (?)' \
                 ,[filename] )
        g.db.commit()
        flash("Download disabled now for this file")
        # return 'Disabled'
    else:
        flash("File already blacklisted")
    return redirect(url_for('show_entries'))


#view history by key
@app.route('/history/<key>', methods=['GET','POST'])
@app.route('/history', methods=['GET','POST'])
def get_history(key):
    if not session.get('logged_in'):
        abort(401)
    if request.method == 'POST':
#       401 g.db.execute('update entries set response=? \
#                      where key=?',(request.form['text'],key))
#        g.db.commit()
#        flash('Entry was successfully edited  ')
        return redirect(url_for('show_entries'))
    elif request.method != 'POST':

        cur = g.db.execute("select downloaded, starttime, endtime, srcip, status, error, file_name from downloads where file_name = " + "'" + key + "'")
        entries = [dict(Download_On=row[0], Start_time=row[1], \
        End_time=row[2], ip=row[3], status=row[4], Error=row[5],
        File_name=row[6] ) for row in cur.fetchall()]
        return render_template('show_history.html', entries=entries)

#RESTful
@app.route('/test_takt/<key>', methods=['GET','POST'])
def rest_key(key):
    cur = g.db.execute("select key, response, id from entries where key = "+ "'" + key + "'")
    entries = [{"key" : row[0], "text" : row[1] } for row in cur.fetchall()]

    js = entries[0]['text'] + "\n"
    resp = Response(js, status=200, mimetype='application/xml')
    resp.headers['Link'] = 'http://localhost'

    g.db.execute('insert into history (key, htime, method, request, response) values (?, ?, ?, ?, ?)',
            (key,datetime.datetime.now().strftime("%D:%H:%M"),request.method,request.url,"Content-type: "
                + resp.headers.get_all('Content-type')[0]
                + "\n" + "Status code: " + str(resp.status_code)
                + "\n" + js + "\n"))
    g.db.commit()

    return resp

@app.route('/Get_Last_Lerg_Time')
def lastLergUpload():
    uploads=getUploads()
    uploads=json.dumps(uploads, ensure_ascii=False)
    resp = Response(uploads, status=200, mimetype='application/json')
    resp.headers['Link'] = 'http://localhost'
    return resp

@app.route('/Get_LatestLerg')
def getLatestLerg():
    cur = g.db.execute("select file_name from (select MAX(uploaded), file_name from uploads)")
    entry = cur.fetchall()[0][0]
    cur.close()

    ip = request.environ.get('HTTP_X_REAL_IP', request.remote_addr)
    dncount=0
    disblcount = checkDisabled(entry)
    dncount=checkDownCnt(entry, ip)

    if dncount>=5:
        resp="Download counti from this IP exceeded. Game over"
        return Response(resp, mimetype="text/plain")

    elif disblcount>0:
        #        abort(304)
        resp="This file disable for download"
        return  Response(resp, mimetype="text/plain")
    else:
        start_time = datetime.datetime.now().strftime("%F-%H:%M:%S")
#        headers = {"Content-Disposition": "attachment; filename=%s" % filename}
     #       with open('./uploads/'+filename, 'r') as f:
     #           body = f.read()
        down_finish_time =  datetime.datetime.now().strftime("%F-%H:%M:%S")
#       return response.make_response((body, headers))
        #ip = request.environ.get('HTTP_X_REAL_IP', request.remote_addr)
        g.db.execute('insert into downloads (downloaded, starttime, \
        endtime,srcip, status, error, file_name) values (?, ?, ?, ?, ?, ?, \
        ?)',[start_time, start_time, down_finish_time, ip,'test_status', 'not error', entry])
        g.db.commit()

        with open("./uploads/"+entry, "r") as f:
            resp = f.read()
        return Response(resp, mimetype="text/plain")



@app.route('/login', methods=['GET','POST'])
def login():
    error = None
    if request.method == 'POST':
        if request.form['username'] != app.config['USERNAME']:
            error = 'Invalid username'
        elif request.form['password'] != app.config['PASSWORD']:
            error = 'Invalid password'
        else:
            session['logged_in'] = True
            flash('You were logged in')
            return redirect(url_for('show_entries'))
    return render_template('login.html', error=error)


@app.route('/logout')
def logout():
    session.pop('logged_in', None)
    flash('You were logged out')
    return redirect(url_for('show_entries'))

@app.before_request
def before_request():
    g.db = connect_db()

@app.teardown_request
def teardown_request(exception):
    db = getattr(g, 'db', None)
    if db is not None:
        db.close()



if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8000, debug=True)
