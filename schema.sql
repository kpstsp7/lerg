drop table if exists uploads;
create table uploads (
  id integer primary key autoincrement,
  uploaded text not null,
  record_cnt integer not null,
  download_cnt integer not null,
  goopath text,
  file_name text not null unique
);
drop table if exists downloads;
create table downloads (
    id integer primary key autoincrement,
    downloaded text not null,
    starttime text not null,
    endtime text not null,
    srcip text not null,
    status boolean not null,
    error text,
    file_name text not null
);
drop table if exists disabled;
create table disabled (
    id integer primary key autoincrement,
    file text not null
);




